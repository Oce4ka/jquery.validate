function sendNewMember() { // длинная
  var
    email = $('[name=mail]').val(),
    street = $('[name=street]').val(),
    home = $('[name=street_number]').val(),
    city = $('[name=city]').val(),
    postcode = $('[name=zip]').val();

  sendToPurinaAdmin(email, street, home, city, postcode, 'long');
};

function sendOldMember() { // короткая
  var
    formType = 'short',
    email = $('[name=mail2]').val(),
    street = $('[name=street2]').val(),
    home = $('[name=street_number2]').val(),
    city = $('[name=city2]').val(),
    postcode = $('[name=zip2]').val();

  console.log('checkIfUserExists()');
  var requestURL = "https://prod2.proplan.co.il/friskies/cms/api/leads"; //TODO: to change link before deployment

  var returningAnswer = '';

  var jqxhr = $.ajax({
    type: "POST",
    url: requestURL,
    data:
      {
        "data": {
          "user_id": "380", // TODO: ??? change
          "email": email
        },
        "jsonDataRequire": {
          "email_email": ''
        }
      },
    beforeSend: function (request) {
      request.setRequestHeader("X-Csrf-Token", "c6aead18097515ffa7a180989aa81b19"); // TODO: change
      var urlstart = requestURL.indexOf('://') + 3;
      var auth = (requestURL.substring(urlstart)).indexOf('www');
      if (auth != 0) {
        request.setRequestHeader("Authorization", "Basic " + btoa("ilvaldfodv:wuabn123DV"));
      }
    },
    success: function (response, xhr) {
      console.log(xhr);
      console.log("Response from Purina Admin Checking User: ", response);
      $('.error-text').detach();
      if (response.status == "You already leeds") { //messageIfExists
        console.log('user exists');
        sendToPurinaAdmin(email, street, home, city, postcode, formType);
      }
    },
    dataType: "json",
    method: "POST",
    statusCode: {
      404: function () {
        console.log("page not found (checking if user exists)");
        $('.error-text').detach();
        $('.popup-content').append('<p class="error-text">כתובת הדוא"ל שהוזנה אינה מזוהה במאגר חברי המועדון. אנא בדקו ונסו שנית.</p>');
        $('.popup').show();
        //показать messageIfNotExists
      }
    }
  });
}

function sendToPurinaAdmin(email, street, home, city, postcode, formType) {
  console.log('sendToPurinaAdmin()');

  // Sending data to purina admin
  console.log('Sending data to purina admin...');

  var requestURL = "https://prod2.proplan.co.il/friskies/cms/api/leads"; //TODO: to change link before deployment
  $.ajax({
    type: "POST",
    url: requestURL,
    data:
      {
        "data": {
          "user_id": "379", // TODO: ??? change
          "email": email,
          "first_name": $('[name=name]').val(),
          "last_name": $('[name=surname]').val(),
          "cell_phone": $('[name=phone]').val(),
          "street": street,
          "home": home,
          "city": city,
          "postcode": postcode,
          "accept_det": "1",
          "source_name": "cats_pouches",
          "ip": $('[name=ip]').val(),
          "databank_num": "700061251"
        },
        "jsonDataRequire": {
          "email_email": email
        }
      },
    beforeSend: function (request) {
      request.setRequestHeader("X-Csrf-Token", "0eee3c69e33b158d4885b11495b521b2"); // TODO: change
      var urlstart = requestURL.indexOf('://') + 3;
      var auth = (requestURL.substring(urlstart)).indexOf('www');
      if (auth != 0) {
        request.setRequestHeader("Authorization", "Basic " + btoa("ilvaldfodv:wuabn123DV"));
      }
    },
    success: function (response, xhr) {
      console.log(xhr);
      console.log("Response from Purina Admin: ", response);

      if (formType == 'long') {
        // что делать после отправки данных в длинную форму
        if (response.status == "You already leeds") {
          $('.error-text').detach();
          $('.popup-content').append('<p class="error-text">כתובת הדוא"ל כבר השתתפה בפעילות, אנא נסו עם כתובת אחרת</p>');
          $('.popup').show();
        } else if (response.status == 'Form sending') {
          $('.thank-you').show();
          dataLayer.push({
            'event': 'LeadComplete'
          });
          console.log('LeadComplete');
        }
      } else if (formType == 'short') {
        // что делать после отправки данных в короткую форму
        if (response.status == "You already leeds") { // лид уже существует
          $('.error-text').detach();
          $('.popup-content').append('<p class="error-text">כתובת הדוא"ל כבר השתתפה בפעילות, אנא נסו עם כתובת אחרת</p>');
          $('.popup').show();
        } else if (response.status == 'Form sending') { // лид не существует и успешно записался
          $('.thank-you').show();
          dataLayer.push({
            'event': 'RegisterLogin'
          });
          console.log('RegisterLogin');
        }
      }
    },
    dataType: "json",
    method: "POST",
    statusCode: {
      404: function () {
        console.log("page not found");
        $('.popup-content').append('<p class="error-text"> תקלה בחיבור, אבל אל חשש!' + 'בדקו את החיבור לרשת ונסו שוב' + '</p>');
        $('.popup').show();
      }
    }
  });
};


// New Member Validation
$("#newMember").pluginValidateForm({
  showAllErrorsInOneSelector: true,
  selectorToHideIfNoErrors: '.popup',
  showErrorsSelector: '.errors',
  showErrorsAfterField: false,
  addClassOnError: 'error',
  errorWrapper: {tag: 'div', class: 'error-text'},
  fields: ["name", "surname", "phone", "mail", "street", "street_number", "city", "zip"],
  onSuccess: sendNewMember
});


// Old Member Validation
$("#oldMember").pluginValidateForm({
  showAllErrorsInOneSelector: true,
  selectorToHideIfNoErrors: '.popup',
  showErrorsSelector: '.errors',
  showErrorsAfterField: false,
  addClassOnError: 'error',
  errorWrapper: {tag: 'div', class: 'error-text'},
  fields: ["mail2", "street2", "street_number2", "city2", "zip2"],
  onSuccess: sendOldMember
});


// Popup behavior
$('.popup .close').click(function (e) {
  e.preventDefault();
  $(e.target).parents('.popup').hide();
  $('.error').first().focus();
  return false;
});


// Forms switching
/*
$(".show-old-member").click(function (e) {
  e.preventDefault()
  $('.tab.new-member').hide();
  $('.tab.old-member').show();
});
$(".show-new-member").click(function (e) {
  e.preventDefault()
  $('.tab.old-member').hide();
  $('.tab.new-member').show();
});
*/

// Accessibility plugin settings
accessibility_rtl = true;
pixel_from_side = 20;
pixel_from_start = 12;
css_style = 2;


// Limit characters for all inputs that have maxlength
$('input').keypress(function (e) {
  var input = $(e.target),
    maxLength = parseInt(input.attr('maxlength')),
    charCode = e.which || e.keyCode;
  var specials = [8, 9, 13, 16, 17, 18, 46]; // Tab, etc. ,19,20,27,33,34,35,36,37,38,39,40,45,46
  if (input.attr('type') == 'number') {
    var inputLength = input.val().toString().length;
  } else {
    var inputLength = input.val().length;
  }
  //console.log('charCode', charCode);
  //console.log($.inArray(charCode, specials));
  //console.log(maxLength);
  //console.log('input', input);
  //console.log('input.val().toString().length', inputLength);
  //console.log('maxLength ', maxLength );
  //console.log('NumericChars', (input.attr('data-pattern') == 'NumericChars'));
  if (maxLength > 0 && ($.inArray(charCode, specials) == -1) && !(inputLength < maxLength)) { // stop on next char after maxlength
    console.log('stopMAX');
    e.preventDefault();
  }
  if ((input.attr('data-pattern') == 'NumericChars') &&
    ($.inArray(charCode, specials) == -1) &&
    !(charCode >= 48 && charCode <= 57)) {
    console.log('stopNAN');
    e.preventDefault();
  }
});


// vertically align forms

(function ($) {
  $.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.height();
  }
})(jQuery);

/*
function formVerticalAlign() {
  //$('.tab').css('margin-top', 0);
  $('.tab').css('margin-top', 0);
  if (!$('.content-form').hasScrollBar()) {
    $('.tab.old-member').css('margin-top', '10vw');
    //$('.tab').css('margin-top', ($('.content-form').outerHeight() - $('.tab').outerHeight() - $('.note-social').outerHeight()) / 2 + 'px')
  }
}
$(window).resize(function () {
  formVerticalAlign();
});
formVerticalAlign();


$(function () {
  formVerticalAlign();
  $(window).resize();
});
*/